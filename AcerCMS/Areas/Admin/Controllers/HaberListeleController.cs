﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;
using AcerCMS.ModelService;

namespace AcerCMS.Areas.Admin.Controllers
{
    public class HaberListeleController : Controller
    {
        // GET: Admin/HaberListele
        public ActionResult HaberListele()
        {
            var model = NewsModelService.GetIndexViewModel();
            return View(model);
            //return View();
        }
    }
}
