﻿using System.Collections.Generic;

namespace AcerCMS.Models
{
    public class CommentViewModel
    {
        public List<Comment> Comments { get; set; }
        public int EntityId { get; set; }
        public EntityType EntityType { get; set; }
    }
}