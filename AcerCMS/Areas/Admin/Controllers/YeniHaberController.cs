﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.Areas.Admin.Controllers
{
    public class YeniHaberController : Controller
    {
        public ActionResult YeniHaber()
        {
            return View();
        }


        [HttpPost]
        public ActionResult YeniHaberEkle(FormCollection form)
        {
            using (var db = new AcerCmsEntities())
            {
                new News() { Title = form["txtBaslik"], ShortDesctription = form["txtKisaTanim"], Content = form["txtHaber"] };
                db.SaveChanges();
            }

            return View();
        }
    }
}