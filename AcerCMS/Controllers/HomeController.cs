﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AcerCMS.Models;

namespace AcerCMS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {                        
            // Commit edilmek için. yazıldı.  2015 12 17 
            var sliderData = new List<Picture>();
            using (var db = new AcerCmsEntities())
            {
                sliderData = db.Pictures.Where(s => s.IsSlider).ToList();
            }

            return View(sliderData);
        }

        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.XXX = "ömer";
            ViewData["Nesne"] = "Örnek Nesne";
            return View();
        }


        public ActionResult AddSomeNews()
        {
            var news = new List<News>()
            {
                new News() {Content = "Bu Bir deneme Haberdir11!", ShortDesctription = "Kısa Açıklama11", Title = "Çoook Önemli bir Haber11", UpdateDate = DateTime.Now.AddHours(1), ViewCount=11},
                new News() {Content = "Bu Bir deneme Haberdir21!", ShortDesctription = "Kısa Açıklama21", Title = "Çoook Önemli bir Haber21", UpdateDate = DateTime.Now.AddHours(2), ViewCount=21},
                new News() {Content = "Bu Bir deneme Haberdir31!", ShortDesctription = "Kısa Açıklama31", Title = "Çoook Önemli bir Haber31", UpdateDate = DateTime.Now.AddHours(3), ViewCount=31},
                new News() {Content = "Bu Bir deneme Haberdir41!", ShortDesctription = "Kısa Açıklama41", Title = "Çoook Önemli bir Haber41", UpdateDate = DateTime.Now.AddHours(4), ViewCount=41},
                new News() {Content = "Bu Bir deneme Haberdir51!", ShortDesctription = "Kısa Açıklama51", Title = "Çoook Önemli bir Haber51", UpdateDate = DateTime.Now.AddHours(5), ViewCount=51},
                new News() {Content = "Bu Bir deneme Haberdir61!", ShortDesctription = "Kısa Açıklama61", Title = "Çoook Önemli bir Haber61", UpdateDate = DateTime.Now.AddHours(6), ViewCount=61},
                new News() {Content = "Bu Bir deneme Haberdir71!", ShortDesctription = "Kısa Açıklama71", Title = "Çoook Önemli bir Haber71", UpdateDate = DateTime.Now.AddHours(7), ViewCount=71},
                new News() {Content = "Bu Bir deneme Haberdir81!", ShortDesctription = "Kısa Açıklama81", Title = "Çoook Önemli bir Haber81", UpdateDate = DateTime.Now.AddHours(8), ViewCount=81},
                new News() {Content = "Bu Bir deneme Haberdir91!", ShortDesctription = "Kısa Açıklama91", Title = "Çoook Önemli bir Haber91", UpdateDate = DateTime.Now.AddHours(9), ViewCount=91}
            };


            using (var db = new AcerCmsEntities())
            {
                foreach (var _news in news)
                {
                    db.News.Add(_news);
                }
                db.SaveChanges();

                var news1Id = news.First().Id;
                var news2Id = news.First().Id;
                var news1 = db.News.SingleOrDefault(x => x.Id == news1Id);
                var news2 = db.News.FirstOrDefault(x => x.Id == news2Id);

                //news1.ShortDesctription = "hahahahahahahaha";

                //db.SaveChanges();
            }


            return null;
        }

        public ActionResult SeedPictures()
        {
            var pictures = new List<Picture>()
            {
                new Picture() {FileName = "1.png", IsSlider = true, Description = "ÇÇÇÇÇLogo".SeoEncode(), Path = "/Content/images/Uploads/Slider",Title = "ÇÇÇÇÇLogo".SeoEncode()},
                new Picture() {FileName = "dd.png", IsSlider = true, Description = "Logo 1", Path = "/Content/images/Uploads/Slider",Title = " askdjhaksj hdkjhskd"},
                new Picture() {FileName = "fm.png", IsSlider = false, Description = "Logo 2 ", Path = "/Content/images/Uploads/Slider",Title = " kasjhd kjahskjdh kjshd"},
                new Picture() {FileName = "ws.png", IsSlider = true, Description = "Logo 3 ", Path = "/Content/images/Uploads/Slider",Title = " kasjhd kjahskjdh kjshczxvcxvd"}
            };

            using (var db = new AcerCmsEntities())
            {
                foreach (var _slider in pictures)
                {
                    db.Pictures.Add(_slider);
                }
                db.SaveChanges();             
            }

            return null;
        }

        #region Testing Form
        //public ActionResult ReadNews(int id)
        //{
        //    var model = new NewsViewModel()
        //    {
        //        News = new News()
        //        {
        //            Id = 1,
        //            Content = "Bu Bir deneme Haberdir!",
        //            ShortDescription = "Kısa Açıklama",
        //            Title = "Çoook Önemli bir Haber"
        //        },
        //        Comments = new List<Comment>()
        //        {
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş",
        //                Id = 1,
        //                Author = "Mesut Talebi",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş2",
        //                Id = 2,
        //                Author = "kdaj kjh kjhkjhs",
        //                PostDate = DateTime.Now.AddHours(1)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş3",
        //                Id = 3,
        //                Author = " sadf asdf sadf sadf asdf",
        //                PostDate = DateTime.Now.AddHours(2)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş4",
        //                Id = 4,
        //                Author = "Mesut Talebiq qw dfsa",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş dfasd fsd f",
        //                Id = 5,
        //                Author = "Mesut Talebi adsfsdf adsf",
        //                PostDate = DateTime.Now
        //            }
        //        }
        //    };

        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult ReadNews(NewsViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        ModelState.AddModelError("", "Lütfen Bilgileri Kontrol ederek tekrar deneyiniz!");

        //        var model1 = new NewsViewModel()
        //        {
        //            News = new News()
        //            {
        //                Id = 1,
        //                Content = "Bu Bir deneme Haberdir!",
        //                ShortDescription = "Kısa Açıklama",
        //                Title = "Çoook Önemli bir Haber"
        //            },
        //            Comments = new List<Comment>()
        //        {
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş",
        //                Id = 1,
        //                Author = "Mesut Talebi",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş2",
        //                Id = 2,
        //                Author = "kdaj kjh kjhkjhs",
        //                PostDate = DateTime.Now.AddHours(1)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş3",
        //                Id = 3,
        //                Author = " sadf asdf sadf sadf asdf",
        //                PostDate = DateTime.Now.AddHours(2)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş4",
        //                Id = 4,
        //                Author = "Mesut Talebiq qw dfsa",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş dfasd fsd f",
        //                Id = 5,
        //                Author = "Mesut Talebi adsfsdf adsf",
        //                PostDate = DateTime.Now
        //            }
        //        }
        //        };

        //        return View(model1);
        //    }


        //    //Insert Logic

        //    ViewBag.Message = "Tebrikler! Kayıt Başarıyla Kaydedilmiştir!";

        //    var model2 = new NewsViewModel()
        //    {
        //        News = new News()
        //        {
        //            Id = 1,
        //            Content = "Bu Bir deneme Haberdir!",
        //            ShortDescription = "Kısa Açıklama",
        //            Title = "Çoook Önemli bir Haber"
        //        },
        //        Comments = new List<Comment>()
        //        {
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş",
        //                Id = 1,
        //                Author = "Mesut Talebi",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş2",
        //                Id = 2,
        //                Author = "kdaj kjh kjhkjhs",
        //                PostDate = DateTime.Now.AddHours(1)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş3",
        //                Id = 3,
        //                Author = " sadf asdf sadf sadf asdf",
        //                PostDate = DateTime.Now.AddHours(2)
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş4",
        //                Id = 4,
        //                Author = "Mesut Talebiq qw dfsa",
        //                PostDate = DateTime.Now
        //            },
        //            new Comment()
        //            {
        //                Content = "çok önemliymiş dfasd fsd f",
        //                Id = 5,
        //                Author = "Mesut Talebi adsfsdf adsf",
        //                PostDate = DateTime.Now
        //            }
        //        }
        //    };

        //    return View(model2);
        //}
        #endregion        
    }
}