﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcerCMS.Filters
{
    public class MergeModelStateAttribute : FilterAttribute, IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Controller.TempData["ModelState"] != null)
            {
                filterContext.Controller.ViewData.ModelState.Merge(filterContext.Controller.TempData["ModelState"] as ModelStateDictionary);
            }
        }
    }
}